package com.example.ioexample.service;

import com.example.ioexample.logger.WEBAPI_Logger;

public interface InitializeInstanceTestService {
    String initializeInstanceTest(WEBAPI_Logger log);
}
