package com.example.ioexample.service;

import java.util.Map;

public interface UserService {

    public Map<String,Object> selectUser() throws Exception;
}
