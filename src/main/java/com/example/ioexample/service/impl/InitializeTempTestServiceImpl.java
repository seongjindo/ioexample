package com.example.ioexample.service.impl;

import com.example.ioexample.logger.WEBAPI_Logger;
import com.example.ioexample.service.InitializeTempTestService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class InitializeTempTestServiceImpl implements InitializeTempTestService {

    private WEBAPI_Logger log = null;

    @Override
    public void logtest(UUID uuid, WEBAPI_Logger log) {
        log = log;

        log.info(uuid+"test3");
        log.info(uuid+"test4");
    }
}
