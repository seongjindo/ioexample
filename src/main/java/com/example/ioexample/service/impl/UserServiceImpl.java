package com.example.ioexample.service.impl;

import com.example.ioexample.mapper.UserMapper;
import com.example.ioexample.service.UserService;
import com.example.ioexample.vo.UserVO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    private UserMapper userMapper;

    private static final String RESULT = "result";
    private static final String LIST = "list";

    public UserServiceImpl(UserMapper userMapper){this.userMapper = userMapper;}
    @Override
    public Map<String, Object> selectUser() throws Exception{
        Map<String, Object> retMap = new HashMap<>();
        retMap.put(LIST, userMapper.selectUser());
        return retMap;
    }
}
