package com.example.ioexample.service.impl;

import com.example.ioexample.logger.WEBAPI_Logger;
import com.example.ioexample.service.InitializeInstanceTestService;
import com.example.ioexample.service.InitializeTempTestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class InitializeInstanceTestServiceImpl implements InitializeInstanceTestService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private WEBAPI_Logger log = null;
    private InitializeTempTestService initializeTempTestService;

    public InitializeInstanceTestServiceImpl(InitializeTempTestService initializeTempTestService){
        this.initializeTempTestService = initializeTempTestService;
    }

    @Override
    public String initializeInstanceTest(WEBAPI_Logger log) {
        log = log;

        UUID uuid = UUID.randomUUID();

        log.info(uuid+"test1");
        log.info(uuid+"test2");
        try{
            Thread.sleep(5000);
        }catch(Exception e){
            e.printStackTrace();
        }
        initializeTempTestService.logtest(uuid, log);
        log.info(uuid+"test5");
        log.info(uuid+"test6");

        log.info(log.tempLogBuffer.toString());

        return "";
    }
}
