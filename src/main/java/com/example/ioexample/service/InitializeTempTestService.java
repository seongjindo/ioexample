package com.example.ioexample.service;

import com.example.ioexample.logger.WEBAPI_Logger;

import java.util.UUID;

public interface InitializeTempTestService {

    void logtest(UUID uuid, WEBAPI_Logger log);
}
