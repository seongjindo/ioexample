package com.example.ioexample.controller;

import com.example.ioexample.logger.WEBAPI_Logger;
import com.example.ioexample.service.InitializeInstanceTestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InitializeInstanceTestController {

    private InitializeInstanceTestService initializeInstanceTestService;

    public InitializeInstanceTestController(InitializeInstanceTestService initializeInstanceTestService){
        this.initializeInstanceTestService = initializeInstanceTestService;
    }
    
    @GetMapping(value="/instance")
    public String initializeInstanceTest(){

        WEBAPI_Logger log = new WEBAPI_Logger();

        initializeInstanceTestService.initializeInstanceTest(log);
        return "OK";
    }

    
}
