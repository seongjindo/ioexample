package com.example.ioexample.controller;

import com.example.ioexample.response.BasicResponse;
import com.example.ioexample.response.CommonResponse;
import com.example.ioexample.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class UserController {

    private final HttpStatus status = HttpStatus.OK;

    private UserService userService;

    public UserController(UserService userService) {this.userService = userService;}

    @GetMapping(value="/getUser")
    public ResponseEntity<? extends BasicResponse> getUser() throws Exception{
        log.info("getUser");
        return new ResponseEntity<>(new CommonResponse<>(userService.selectUser()), status);
    }
}
