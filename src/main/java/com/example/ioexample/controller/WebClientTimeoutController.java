package com.example.ioexample.controller;

import io.netty.channel.ChannelOption;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

@RestController
@Slf4j
public class WebClientTimeoutController {

    private final WebClient webClient;

    public WebClientTimeoutController(){
        int connectTimeoutMillis = 5000;

        HttpClient httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectTimeoutMillis);

        this.webClient = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl("http://localhost:8080")
                .build();
    }

    @GetMapping(value="/external-call")
    public String makeExternalCall(){
        return this.webClient.get().uri("/").retrieve().bodyToMono(String.class).block();
    }
}
