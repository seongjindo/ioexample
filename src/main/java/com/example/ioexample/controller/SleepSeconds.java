package com.example.ioexample.controller;


import com.example.ioexample.response.BasicResponse;
import com.example.ioexample.response.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping(value="/sleep")
public class SleepSeconds {

    private final HttpStatus status = HttpStatus.OK;

    @GetMapping(value="/time")
    public ResponseEntity<? extends BasicResponse> sleep() throws Exception{
        System.out.println("inbound request");
        Thread.sleep(3000);
        return new ResponseEntity<>(new CommonResponse<>("sucess"),status);
    }
}
