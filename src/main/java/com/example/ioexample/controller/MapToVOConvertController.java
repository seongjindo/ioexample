package com.example.ioexample.controller;

import com.example.ioexample.response.BasicResponse;
import com.example.ioexample.response.CommonResponse;
import com.example.ioexample.vo.ConvertVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class MapToVOConvertController {

    @RequestMapping(value="/voConvert")
    public ResponseEntity<? extends BasicResponse> voConvert(HttpServletRequest request){

        Map<String,String> map = new HashMap<>();
        map.put("Test1", request.getParameter("TEST1"));
        map.put("test2", request.getParameter("test2"));
        map.put("test3", request.getParameter("test3"));
        map.put("test4", request.getParameter("test4"));

        ObjectMapper mapper = new ObjectMapper();
        ConvertVO vo = mapper.convertValue(map, ConvertVO.class);
        System.out.println(map);
        System.out.println(vo.toString());



        return new ResponseEntity<>(new CommonResponse<>("OK"), HttpStatus.OK);
    }


}
