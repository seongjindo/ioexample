package com.example.ioexample.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

public class WEBAPI_Logger implements Logger {

    private static Logger log = LoggerFactory.getLogger(WEBAPI_Logger.class);
    public StringBuffer tempLogBuffer = new StringBuffer("");

    @Override
    public String getName() {
        return "";
    }

    @Override
    public boolean isTraceEnabled() {
        return false;
    }

    @Override
    public void trace(String s) {

    }

    @Override
    public void trace(String s, Object o) {

    }

    @Override
    public void trace(String s, Object o, Object o1) {

    }

    @Override
    public void trace(String s, Object... objects) {

    }

    @Override
    public void trace(String s, Throwable throwable) {

    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return false;
    }

    @Override
    public void trace(Marker marker, String s) {

    }

    @Override
    public void trace(Marker marker, String s, Object o) {

    }

    @Override
    public void trace(Marker marker, String s, Object o, Object o1) {

    }

    @Override
    public void trace(Marker marker, String s, Object... objects) {

    }

    @Override
    public void trace(Marker marker, String s, Throwable throwable) {

    }

    @Override
    public boolean isDebugEnabled() {
        return false;
    }

    @Override
    public void debug(String s) {
        log.debug(s);
    }

    @Override
    public void debug(String s, Object o) {
        log.debug(s,o);
    }

    @Override
    public void debug(String s, Object o, Object o1) {
        log.debug(s,o,o1);
    }

    @Override
    public void debug(String s, Object... objects) {
        log.debug(s, objects);
    }

    @Override
    public void debug(String s, Throwable throwable) {
        log.debug(s, throwable);
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return false;
    }

    @Override
    public void debug(Marker marker, String s) {
        log.debug(marker, s);
    }

    @Override
    public void debug(Marker marker, String s, Object o) {
        log.debug(marker, s, o);
    }

    @Override
    public void debug(Marker marker, String s, Object o, Object o1) {
        log.debug(marker, s, o, o1);
    }

    @Override
    public void debug(Marker marker, String s, Object... objects) {
        log.debug(marker, s, objects);
    }

    @Override
    public void debug(Marker marker, String s, Throwable throwable) {
        log.debug(marker, s, throwable);
    }

    @Override
    public boolean isInfoEnabled() {
        return false;
    }

    @Override
    public void info(String s) {
        log.info(s);
        tempLogBuffer.append(s).append("\n");
    }

    @Override
    public void info(String s, Object o) {
        log.info(s, o);
        tempLogBuffer.append(s).append(o).append("\n");
    }

    @Override
    public void info(String s, Object o, Object o1) {
        log.info(s, o, o1);
        tempLogBuffer.append(s).append(o).append(o1).append("\n");
    }

    @Override
    public void info(String s, Object... objects) {
        log.info(s, objects);
        tempLogBuffer.append(s);
        for(Object obj: objects){
            tempLogBuffer.append(obj).append(", ");
        }
        tempLogBuffer.append("\n");
    }

    @Override
    public void info(String s, Throwable throwable) {
        log.info(s, throwable);
        tempLogBuffer.append(s).append(throwable).append("\n");
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return false;
    }

    @Override
    public void info(Marker marker, String s) {
        log.info(marker, s);
    }

    @Override
    public void info(Marker marker, String s, Object o) {
        log.info(marker, s, o);
    }

    @Override
    public void info(Marker marker, String s, Object o, Object o1) {
        log.info(marker, s,o,o1);
    }

    @Override
    public void info(Marker marker, String s, Object... objects) {
        log.info(marker, s, objects);
    }

    @Override
    public void info(Marker marker, String s, Throwable throwable) {
        log.info(marker, s, throwable);
    }

    @Override
    public boolean isWarnEnabled() {
        return false;
    }

    @Override
    public void warn(String s) {

    }

    @Override
    public void warn(String s, Object o) {

    }

    @Override
    public void warn(String s, Object... objects) {

    }

    @Override
    public void warn(String s, Object o, Object o1) {

    }

    @Override
    public void warn(String s, Throwable throwable) {

    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return false;
    }

    @Override
    public void warn(Marker marker, String s) {

    }

    @Override
    public void warn(Marker marker, String s, Object o) {

    }

    @Override
    public void warn(Marker marker, String s, Object o, Object o1) {

    }

    @Override
    public void warn(Marker marker, String s, Object... objects) {

    }

    @Override
    public void warn(Marker marker, String s, Throwable throwable) {

    }

    @Override
    public boolean isErrorEnabled() {
        return false;
    }

    @Override
    public void error(String s) {
        log.error(s);
        tempLogBuffer.append(s).append("\n");
    }

    @Override
    public void error(String s, Object o) {
        log.error(s, o);
        tempLogBuffer.append(s).append(o).append("\n");
    }

    @Override
    public void error(String s, Object o, Object o1) {
        log.error(s, o, o1);
        tempLogBuffer.append(s).append(o).append(o1).append("\n");
    }

    @Override
    public void error(String s, Object... objects) {
        log.error(s, objects);
        tempLogBuffer.append(s);
        for(Object obj : objects){
            tempLogBuffer.append(obj).append(", ");
        }
        tempLogBuffer.append("\n");
    }

    @Override
    public void error(String s, Throwable throwable) {
        log.error(s, throwable);
        tempLogBuffer.append(s).append(throwable).append("\n");
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return false;
    }

    @Override
    public void error(Marker marker, String s) {
        log.error(marker, s);
    }

    @Override
    public void error(Marker marker, String s, Object o) {
        log.error(marker,s,o);
    }

    @Override
    public void error(Marker marker, String s, Object o, Object o1) {
        log.error(marker, s, o, o1);
    }

    @Override
    public void error(Marker marker, String s, Object... objects) {
        log.error(marker, s, objects);
    }

    @Override
    public void error(Marker marker, String s, Throwable throwable) {
        log.error(marker, s, throwable);
    }
}
