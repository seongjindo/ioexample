package com.example.ioexample.mapper;

import com.example.ioexample.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    List<UserVO> selectUser();
}
