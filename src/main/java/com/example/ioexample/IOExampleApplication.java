package com.example.ioexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IOExampleApplication {

    static{
        System.setProperty("spring.config.location","classpath:/application.yaml");
    }
    public static void main(String[] args) {
        SpringApplication.run(IOExampleApplication.class, args);
    }

}
