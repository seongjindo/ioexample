package com.example.ioexample.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse implements BasicResponse {
    private String errorMessage;
    private String errorCode;

    public ErrorResponse(String errorMessage){
        this.errorMessage = errorMessage;
        this.errorCode = "404";
    }

    public ErrorResponse(String errorCode, String errorMessage){
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }
}
