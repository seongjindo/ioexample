package Benchmark;

import lombok.Data;

public class ReflectionBenchmark {
    public static void main(String[] ar) throws Exception{
        CommonVO vo = new CommonVO();
        vo.setField1("value1");
        vo.setField2("value2");
        vo.setField3("value3");
        String[] fieldList = {"field1", "field2", "field3"};

        for (int i=0; i< 10000; i++){
            directAccess(vo, fieldList);
            reflectionAccess(vo, fieldList);
        }

        long startTime = System.currentTimeMillis();
        for(int i=0; i<1000000; i++){
            directAccess(vo, fieldList);
        }
        long endTime = System.currentTimeMillis();
        long directAccessTime = endTime - startTime;

        startTime = System.currentTimeMillis();
        for(int i=0; i<1000000; i++){
            reflectionAccess(vo, fieldList);
        }
        endTime = System.currentTimeMillis();
        long reflectionAccessTime = endTime - startTime;

        System.out.println("Direct AccessTime: " + directAccessTime + " ms");
        System.out.println("Reflection AccessTime: " + reflectionAccessTime + " ms");
    }

    public static void directAccess(CommonVO vo, String[] fieldList){
        for(String field: fieldList){
            if(field.equals("field1")){
                vo.getField1();
            }else if(field.equals("field2")){
                vo.getField2();
            }else if(field.equals("field3")){
                vo.getField3();
            }
        }
    }

    public static void reflectionAccess(CommonVO vo, String[] fieldList) throws Exception{
        for(String field: fieldList){
            java.lang.reflect.Field f = CommonVO.class.getDeclaredField(field);
            f.setAccessible(true);
            Object value = f.get(vo);
        }
    }

}

@Data
class CommonVO{
    private String field1;
    private String field2;
    private String field3;
}