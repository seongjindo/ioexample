package deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.util.HashMap;
import java.util.Map;

public class MapValueConvertor {

    public static void main(String[] ar){

        ObjectMapper mapper = new ObjectMapper();
//        mapper.setPropertyNamingStrategy(PropertyNamingStrategies.);
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Map.class, new CustomDeserializer());
        mapper.registerModule(module);

        Map<String, Object> originalMap = new HashMap<>();
        originalMap.put("MID","test1");
        originalMap.put("BankCode", "020");
        originalMap.put("EdiDate", "slkjdlfkslkdhg");

        CustomVO vo = mapper.convertValue(originalMap, CustomVO.class);
        System.out.println(vo.toString());

    }
}
