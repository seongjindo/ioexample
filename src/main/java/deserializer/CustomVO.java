package deserializer;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CustomVO {
    private String MID;
    private String BankCode;
    private String EdiData;
}
