package deserializer;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class CustomDeserializer extends JsonDeserializer<Map<String,Object>> {

    @Override
    public Map<String, Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        Map<String, Object> originalMap = jsonParser.readValueAs(Map.class);
        Map<String, Object> transformedMap = new HashMap<>();
        for(Map.Entry<String,Object> entry : originalMap.entrySet()){
            transformedMap.put(entry.getKey().toLowerCase(), entry.getValue());
        }

        log.info("transformedMap: "+transformedMap);
        return transformedMap;
    }
}
