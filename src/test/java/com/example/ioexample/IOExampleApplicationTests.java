package com.example.ioexample;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class IOExampleApplicationTests {

    private static final String URL = "http://localhost:8080/sleep/time";
    private static final String URL_USER = "http://localhost:8080/getUser";
    private static final int LOOP_COUNT = 300;

    private final WebClient webClient = WebClient.create();
    private final CountDownLatch count = new CountDownLatch(LOOP_COUNT);

    @Test
    public void blocking(){
        RestTemplate restTemplate = new RestTemplate();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        for(int i=1; i<=LOOP_COUNT; i++){
            System.out.println("Request count : "+i);
            final ResponseEntity<String> response =
                    restTemplate.exchange(URL, HttpMethod.GET, HttpEntity.EMPTY,String.class);
            assertThat(response.getBody().contains("success"));
            System.out.println(response.getBody());
        }
        stopWatch.stop();
        System.out.print(stopWatch.prettyPrint());
    }

    @Test
    public void blocking_user(){
        //Use restTemplate and request URL_USER
        RestTemplate restTemplate = new RestTemplate();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for(int i=1; i<=LOOP_COUNT; i++){
            System.out.println("Request count : "+i);
            final ResponseEntity<String> response =
                    restTemplate.exchange(URL_USER, HttpMethod.GET, HttpEntity.EMPTY, String.class);
            assertThat(response.getBody().contains("sjdo"));
            System.out.println(response.getBody());
        }
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }

    @Test
    public void nonBlocking() throws Exception{
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for(int i=1; i<=LOOP_COUNT; i++){
            System.out.println("Request count : "+i);
            this.webClient
                    .get()
                    .uri(URL)
                    .retrieve()
                    .bodyToMono(String.class)
                    .subscribe(it -> {
                        count.countDown();
                        System.out.println(it);
                    });
        }

        count.await(10, TimeUnit.SECONDS);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }

    @Test
    public void nonBlocking_user() throws Exception{
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for(int i=1; i<=LOOP_COUNT; i++){
            System.out.println("Request count : "+i);
            this.webClient
                    .get()
                    .uri(URL_USER)
                    .retrieve()
                    .bodyToMono(String.class)
                    .subscribe(it -> {
                       count.countDown();
                       System.out.println(it);
                    });
        }

        count.await(10, TimeUnit.SECONDS);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }
}
